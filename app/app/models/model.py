from datetime import datetime
from datetime import timedelta
from passlib.apps import custom_app_context as pwd_context

from app import db


class DD(db.Model):
    __tablename__='zp_dd'
    id = db.Column(db.Integer, primary_key=True)
    dd_name =db.Column(db.String(50), nullable=True)
    province=db.Column(db.String(50), nullable=True)
    pointx=db.Column(db.Float, nullable=True)
    pointy=db.Column(db.Float, nullable=True)

class Zwmc(db.Model):
    __tablename__='zp_zwmc'
    id = db.Column(db.Integer, primary_key=True)
    zwmc_name =db.Column(db.String(100), nullable=True)
    list=db.relationship('List',backref='itszwmc')

class Gsmc(db.Model):
    __tablename__='zp_gsmc'
    id = db.Column(db.Integer, primary_key=True)
    gsmc_name =db.Column(db.String(100), nullable=True)
    list=db.relationship('List',backref='itsgsmc')

class Zwlb(db.Model):
    __tablename__='zp_zwlb'
    id = db.Column(db.Integer, primary_key=True)
    zwlb_name =db.Column(db.String(100), nullable=True)
    zwlb_big_id=db.Column(db.Integer,db.ForeignKey('zp_zwlb_big.id'))
    list=db.relationship('List',backref='itszwlb')
    flxx=db.relationship('Flxx',backref='itsflxx')

class ZwlbBig(db.Model):
    __tablename__='zp_zwlb_big'
    id = db.Column(db.Integer, primary_key=True)
    zwlb_big_name =db.Column(db.String(100), nullable=True)
    zwlb_big=db.relationship('Zwlb',backref='bigzwlb')
    list=db.relationship('List',backref='itsbigzwlb')


class Gshy(db.Model):
    __tablename__='zp_gshy'
    id = db.Column(db.Integer, primary_key=True)
    gshy_name =db.Column(db.String(100), nullable=True)
    list=db.relationship('List',backref='itsgshy')

class Gsxz(db.Model):
    __tablename__='zp_gsxz'
    id = db.Column(db.Integer, primary_key=True)
    gsxz_name =db.Column(db.String(100), nullable=True)
    list=db.relationship('List',backref='itsgsxz')

class Gzjy(db.Model):
    __tablename__='zp_gzjy'
    id = db.Column(db.Integer, primary_key=True)
    gzjy_name =db.Column(db.String(100), nullable=True)
    list=db.relationship('List',backref='itsgzjy')

class Xl(db.Model):
    __tablename__='zp_xl'
    id = db.Column(db.Integer, primary_key=True)
    xl_name =db.Column(db.String(100), nullable=True)
    list=db.relationship('List',backref='itsxl')

class Gsgm(db.Model):
    __tablename__='zp_gsgm'
    id = db.Column(db.Integer, primary_key=True)
    gsgm_name =db.Column(db.String(100), nullable=True)
    list=db.relationship('List',backref='itsgsgm')

class List(db.Model):
    __tablename__ = 'zp_list'
    id = db.Column(db.Integer, primary_key=True)
    zwmc_id = db.Column(db.Integer,db.ForeignKey('zp_zwmc.id'))
    gsmc_id = db.Column(db.Integer,db.ForeignKey('zp_gsmc.id'))
    min_zwyx = db.Column(db.Integer, nullable=True)
    max_zwyx = db.Column(db.Integer,nullable=True)
    dd_id = db.Column(db.Integer,db.ForeignKey('zp_dd.id'))
    fbrq = db.Column(db.String(100), nullable=True)
    gsxz_id = db.Column(db.Integer,db.ForeignKey('zp_gsxz.id'))
    gzjy_id = db.Column(db.Integer,db.ForeignKey('zp_gzjy.id'))
    xl_id = db.Column(db.Integer,db.ForeignKey('zp_xl.id'))
    zwlb_id = db.Column(db.Integer,db.ForeignKey('zp_zwlb.id'))
    zwlb_big_id=db.Column(db.Integer,db.ForeignKey('zp_zwlb_big.id'))
    gsgm_id = db.Column(db.Integer,db.ForeignKey('zp_gsgm.id'))
    gshy_id = db.Column(db.Integer,db.ForeignKey('zp_gshy.id'))
    href = db.Column(db.String(100), nullable=False)

class ListOld(db.Model):
    __tablename__ = 'zp_list_old'
    id = db.Column(db.Integer, primary_key=True)
    zwmc = db.Column(db.String(300), nullable=True)
    gsmc = db.Column(db.String(300), nullable=True)
    min_zwyx = db.Column(db.Integer, nullable=True)
    max_zwyx = db.Column(db.Integer,nullable=True)
    dd = db.Column(db.String(100),nullable=True)
    fbrq = db.Column(db.String(100), nullable=True)
    gsxz = db.Column(db.String(100), nullable=True)
    gzjy = db.Column(db.String(100), nullable=True)
    xl = db.Column(db.String(100), nullable=True)
    zwlb = db.Column(db.String(100), nullable=True)
    gsgm = db.Column(db.String(100), nullable=True)
    gshy = db.Column(db.String(100), nullable=False)
    rzyq = db.Column(Text, nullable=True)
    href = db.Column(db.String(100), nullable=False)
    zwlb_big=db.Column(db.String(100), nullable=True)

class RzyqFenCi(db.Model):
    __tablename__='zp_rzyq_fenci'
    id = db.Column(db.Integer, primary_key=True)
    fenci=db.Column(db.String(100), nullable=False)
    list_id=db.Column(db.Integer,db.ForeignKey('zp_list.id'))
    zwlb_id=db.Column(db.Integer,db.ForeignKey('zp_zwlb.id'))

class Flxx(db.Model):
    __tablename__='zp_flxx'
    id = db.Column(db.Integer, primary_key=True)
    flxx_name=db.Column(db.String(100), nullable=False)
    list_id=db.Column(db.Integer,db.ForeignKey('zp_list.id'))
    zwlb_id=db.Column(db.Integer,db.ForeignKey('zp_zwlb.id'))
