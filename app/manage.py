#!/usr/bin/python
#-*-coding:utf-8-*-
# 启动程序
from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager
from app import create_app, db

app = create_app('production')
migrate = Migrate(app,db)
manager = Manager(app)
manager.add_command('db',MigrateCommand)


if __name__ == '__main__':
    manager.run()
