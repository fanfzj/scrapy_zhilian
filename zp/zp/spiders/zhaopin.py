# -*- coding: utf-8 -*-
import json
import re
from urllib.parse import quote

import scrapy
from zp.items import ZpItem


class ZhaopinSpider(scrapy.Spider):
    name = 'zhaopin'  #爬虫名
    allowed_domains = ['zhaopin.com', 'fe-api.zhaopin.com', 'jobs.zhaopin.com']  # 允许访问的域名
    start_urls = ['https://www.zhaopin.com/']  # 进口链接
    headers = {
        'Accept': 'application/json, text/plain, */*',

        'Host': 'fe-api.zhaopin.com',
        'Origin': 'https://sou.zhaopin.com',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.63 Safari/537.36 Qiyu/2.1.1.1'
    }
    job_headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.63 Safari/537.36 Qiyu/2.1.1.1'
    }
    def parse(self, response):
        sel=scrapy.Selector(response)
        div_list = sel.xpath('//div[@class="zp-jobNavigater-popContainer"]')
        for div_item in div_list:
            zwlb_big = div_item.xpath('div[@class="zp-jobNavigater-pop-title"]/text()').extract_first()
            for zwlb in div_item.xpath('div[@class="zp-jobNavigater-pop-list"]/a/span/text()').extract():
                url = 'https://fe-api.zhaopin.com/c/i/sou?start=0&pageSize=60&cityId=489&workExperience=-1&education=-1&companyType=-1&employmentType=-1&jobWelfareTag=-1&kw={}&kt=3'.format(quote(zwlb))
                yield scrapy.Request(url=url, callback=self.parse_list, dont_filter=True,
                                     meta={'zwlb_big': zwlb_big, 'zwlb': zwlb, 'p': 1, 'size': 60, 'start': 60},
                                     headers=self.headers)

    #通过列表页面获取详细页面链接，并完成分页处理
    def parse_list(self,response):
        js = json.loads(response.body_as_unicode())
        self.job_headers['referer'] = 'https://sou.zhaopin.com/?jl=489&kw={}&kt=3'.format(quote(response.meta['zwlb']))
        for i in js['data']['results']:
            job = {}
            zwmc = i['jobName']  # zwmc
            zwlb_big = i['jobType']['items'][0]['name']  # zwlb_big
            zwlb = i['jobType']['items'][1]['name']  # zwlb
            gsmc = i['company']['name']  # gsmc
            gsgm = i['company']['size']['name']  # gsgm
            gsxz = i['company']['type']['name']  # gsxz
            gzjy = i['workingExp']['name']  # gzjy
            zdxl = i['eduLevel']['name']  # zdxl
            zwyx = i['salary']
            job['emplType'] = i['emplType']
            gzdd = i['city']['items'][0]['name']
            fbrq = i['updateDate']
            flxx = i['welfare']

            pattern = re.compile('(\d+)K-(\d+)K').findall(zwyx)
            if pattern:
                min_zwyx = pattern[0][0] + '000'
                max_zwyx = pattern[0][1] + '000'
            else:
                min_zwyx = max_zwyx = 0

            item_one = ZpItem()
            item_one['zwmc']=zwmc
            item_one['gsmc'] = gsmc
            item_one['flxx'] = flxx
            item_one['min_zwyx'] = min_zwyx
            item_one['max_zwyx'] = max_zwyx
            item_one['gzdd'] = gzdd
            item_one['fbrq'] = fbrq
            item_one['gsxz'] = gsxz
            item_one['gzjy'] = gzjy
            item_one['zdxl'] = zdxl
            item_one['zwlb'] = zwlb
            item_one['gsgm'] = gsgm
            item_one['zwlb_big'] = zwlb_big
            url = i['positionURL']
            if url:
                yield scrapy.Request(url=url, callback=self.parse_info, dont_filter=True, meta={'item': item_one},
                                     headers=self.job_headers)

        num = js['data']['numFound']
        if num > response.meta['start']:
            p = response.meta['p'] + 1
            url2 = 'https://fe-api.zhaopin.com/c/i/sou?start={0}&pageSize={1}&cityId=489&workExperience=-1&education=-1&companyType=-1&employmentType=-1&jobWelfareTag=-1&kw={2}&kt=3'.format(
                response.meta['start'], response.meta['size'], response.meta['zwlb'])
            start = response.meta['start'] + 60
            yield scrapy.Request(url=url2, callback=self.parse_list, dont_filter=True,
                                 meta={'zwlb_big': response.meta['zwlb_big'], 'zwlb': response.meta['zwlb'], 'p': p,
                                       'size': response.meta['size'], 'start': start},
                                 headers=self.headers)

    # 职位详细信息
    def parse_info(self, response):
        item = response.meta['item']
        ul_li = response.css('ul.terminal-ul li')
        info_dict = {}
        for li_item in ul_li:
            # decode 解码为unicode
            span_one = li_item.xpath('span/text()').extract_first().strip("：")
            # string 表示获取strong下的所有文本，不管下面的节点包含关系
            # 会有缺点，他会保留所有的空格
            strong_one = li_item.xpath('string(strong)').extract_first()
            # 对strong_one的文本中的空格进行处理
            strong_list = [one.strip() for one in strong_one.split()]
            strong_one = ''.join(strong_list)
            info_dict[span_one] = strong_one
        item['gshy'] = info_dict.get('公司行业', '')
        item['href'] = response.url
        zprs=response.xpath('//div[@class="info-three l"]/span[4]/text()').extract_first('').strip('招人')
        item['zprs'] =zprs if zprs else info_dict.get('招聘人数', '').strip('人')
        item['rzyq'] = response.xpath('string(//div[@class="pos-ul"]|//div[@class="tab-inner-cont"])').extract_first()
        #print(item)
        return item
