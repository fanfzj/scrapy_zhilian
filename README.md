# 简介
基于Python和Echarts职位画像系统，使用Scrapy完整智联招聘职位数据的抓取，使用flask+echarts完成数据可视化，同时使用matplotlib完成基础数据可视化。

# 效果展示
[效果展示](http://zwhx.mdzimu.com/)
